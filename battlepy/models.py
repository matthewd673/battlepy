import json

class Match():
  
    def __init__(self, matchJson, s):
        #test
        print("\n" + s.baseUrl + "\n")
        
        #get the id right away
        self.id = matchJson["data"]["id"]

        #set the useful attributes (consider weeding out unnecessary ones)
        self.creationTime = matchJson["data"]["attributes"]["createdAt"] #todo: parse this time
        self.duration = matchJson["data"]["attributes"]["duration"]
        self.mode = matchJson["data"]["attributes"]["gameMode"] #todo: what does that random string of numbers mean?
        self.version = matchJson["data"]["attributes"]["patchVersion"]
        self.map = matchJson["data"]["attributes"]["stats"]["mapID"] #todo: what does that randoms string of numbers mean?
        self.type = matchJson["data"]["attributes"]["stats"]["type"] #honestly probably doesn't need further parsing
        self.title = matchJson["data"]["attributes"]["titleId"] #not super useful afaik

        #is assets even useful?

        #rosters
        #will have to create array of separate Roster class

        #rounds
        #will have to create array of separate Rounds class

        #spectators
        #haven't ever seen this populated, but will need its own class