import requests
import json

from battlepy import models

class UserSettings():
    def __init__(self, apiKey, datacenter, region):
        self.key = apiKey
        self.dc = datacenter
        self.r = region
        self.baseUrl = "https://api." + self.dc + ".gamelockerapp.com/"
        self.accept = "application/vnd.api+json"
        self.h = { "Authorization": self.key, "Accept": self.accept }

class Battlerite():
    
    def __init__(self, apiKey, region = "global", datacenter = "dc01"):
        self.settings = UserSettings(apiKey, region, datacenter)
    
    #set up:
    #optional, defaults to dc01 (the only one currently)
    def setDatacenter(self, datacenter):
        self.settings.dc = datacenter
        self.settings.baseUrl = "https://api." + datacenter + ".gamelockerapp.com/"
    
    #do this before everything else
    def authenticate(self, apiKey):
        self.settings.key = apiKey
        self.settings.h = { "Authorization": apiKey, "Accept": self.settings.accept }
    
    #optional, defaults to global
    def setRegion(self, region):
        self.settings.r = region
        
    #returning data:
    def status(self): #data is not parsed
        s = self.settings
        url = s.baseUrl + "status"
        req = requests.get(url, headers=s.h)
        print(req.text)
        
    #add support for infinite filters
    def getMatches(self, pageLimit = 5, pageOffset = 0, sort = ""):
        s = self.settings
        url = s.baseUrl + "shards/" + s.r + "/matches?page[limit]=" + str(pageLimit) + "&page[offset]=" + str(pageOffset)
        if(sort != ""):
            url = url + "&sort=" + sort
        req = requests.get(url, headers=s.h)
        print(req.text)
        
    
    def getMatch(self, matchId):
        s = self.settings
        url = s.baseUrl + "shards/" + s.r + "/matches/" + str(matchId)
        req = requests.get(url, headers=s.h)
        matchJson = json.loads(req.text)
        return models.Match(matchJson, s)
        #print(matchJson["data"]["attributes"]["stats"]["type"])
        
    #players have not yet been implemented in the API
    def getPlayer(self, playerId):
        s = self.settings
        url = s.baseUrl + "shards/" + s.r + "/players/" + str(playerId)
        req = requests.get(url, headers=s.h)
        print(req.text)
        
    #needs filters too
    def getPlayers(self):
        s = self.settings
        url = s.baseUrl + "shards/" + s.r + "/players/"
        req = requests.get(url, headers=s.h)
        print(req.text)