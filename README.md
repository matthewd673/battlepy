# battlepy

A painless way to interact with the Battlerite developer API. Consider this project in alpha, as it is still undergoing significant changes.

**_Full documentation coming super soon_**

## Battlerite's API

In order to make use of battlepy you will need to register a developer API key at https://developer.battlerite.com.
Documentation on Battlerite's API is available at http://battlerite-docs.readthedocs.io/.

## Currently on the TODO list:

- Implement filters
- Adding variables to make sorting, setting regions, and setting datacenters easier
- Improve the output (something other than a string of raw JSON in console :/ )
- Settle on a nice way to structure the package

## Warnings

- The whole thing is still structurally a bit of a mess
- Make sure you `setRegion()` to `global` before requesting match data (I'm unsure if this is an intentional limitation of the API)
- Player data is currently unavailable from the API and will return an error when queried. This is on Stunlock's end and should be coming soon